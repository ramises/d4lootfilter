#This script locates the image stickman.png in the region we give it and tell you if it can see it

from pyautogui import * 
import pyautogui 
import time 
import keyboard 
import random
import win32api, win32con
import os
import time
import winsound
from enum import Enum
import constants
from constants import ItemType

# wait 0.1s is ok/

def screen_contains_image(image_path):
    if pyautogui.locateOnScreen(image_path,region=(870,80,1020,800), grayscale=True, confidence=0.95) != None:
        return True
    return False

def move_mouse_outside(x, y, xrand, yrand):
    win32api.SetCursorPos((x + xrand, y + yrand))
    time.sleep(0.1)

def smoothly_move_mouse(x, y, xrand, yrand):
    win32api.SetCursorPos((x + xrand, y + yrand))
    for i in range (5):
        time.sleep(0.01)
        win32api.SetCursorPos((round(x - 2*xrand*(float(i)/5.)), round(y - 2*yrand*(float(i)/5.))))

def generate_rand_shift():
    signal = 1 if random.random() < 0.5 else -1
    value = random.randrange(3, 8)
    return signal*value

def is_worth_keeping(keep_legendary:bool, affixes:list, min_hits:int, debug:bool=False):
    # if is unique
    if screen_contains_image(constants.unique_rarity_path):
        if(debug):
            print("Unique detected")
        return True
    # if is legendary
    # if (keep_legendary and screen_contains_image(constants.legendary_rarity_path)):
    #     if(debug):
    #         print("Legendary detected")
    #     return True
    # test affixes
    hits = 0
    for affix in affixes:
        filePath = constants.affixes_path + '/' + affix + '.png'
        if screen_contains_image(filePath):
            print("Affix detected: ", affix)
            hits += 1
    if hits >= min_hits:
        print("Hits: ", hits)
        return True
    return False

def find_type(previousType:ItemType):
    initial_value = previousType.value
    for i in range (initial_value, 9):
        item_type = ItemType(i)
        file_path = constants.item_type_image_dict[item_type]
        if screen_contains_image(file_path):
            return item_type
    return ItemType.gem

def scan_inventory(keep_legendary:bool, affixes_dict:dict, min_hits_dict:dict, debug:bool=False):
    item_type = ItemType.helm
    for j in range (3):
        for i in range (11):
            if (debug):
                print("Position ", j, i)
            xrand = generate_rand_shift()
            yrand = generate_rand_shift()
            smoothly_move_mouse(1290, 1050, xrand, yrand)
            for k in range (5):
                xrand = generate_rand_shift()
                yrand = generate_rand_shift()
                x = 1290  + 55*(i)
                y = 750 + 85*(j)
                smoothly_move_mouse(x, y, xrand, yrand)
                time.sleep(0.2)
                new_type = find_type(item_type)
                if new_type != ItemType.gem:
                    break    
            item_type = new_type
            if item_type == ItemType.gem:
                break
            if not is_worth_keeping(keep_legendary, affixes_dict[item_type], min_hits_dict[item_type], debug):
                keyboard.press_and_release('space')
            win32api.SetCursorPos((0, 0))
            time.sleep(0.1)
