from enum import Enum

class ItemType(Enum):
    helm = 1
    chest = 2
    weapon = 3
    gloves = 4
    pants = 5
    boots = 6
    ring = 7
    amulet = 8
    gem = 9

item_type_image_dict = {
    ItemType.helm: 'type/helm.png',
    ItemType.chest: 'type/chest.png',
    ItemType.weapon: 'type/weapon.png',
    ItemType.gloves: 'type/gloves.png',
    ItemType.pants: 'type/pants.png',
    ItemType.boots: 'type/boots.png',
    ItemType.ring: 'type/ring.png',
    ItemType.amulet: 'type/amulet.png'
}

item_type_config_dict = {
    ItemType.helm: 'config/helm.txt',
    ItemType.chest: 'config/chest.txt',
    ItemType.weapon: 'config/weapon.txt',
    ItemType.gloves: 'config/gloves.txt',
    ItemType.pants: 'config/pants.txt',
    ItemType.boots: 'config/boots.txt',
    ItemType.ring: 'config/ring.txt',
    ItemType.amulet: 'config/amulet.txt'
}

item_type_affixes_list_dict = {
    ItemType.helm: 'affixes_lists/helm.txt',
    ItemType.chest: 'affixes_lists/chest.txt',
    ItemType.weapon: 'affixes_lists/weapon.txt',
    ItemType.gloves: 'affixes_lists/gloves.txt',
    ItemType.pants: 'affixes_lists/pants.txt',
    ItemType.boots: 'affixes_lists/boots.txt',
    ItemType.ring: 'affixes_lists/ring.txt',
    ItemType.amulet: 'affixes_lists/amulet.txt'
}

affixes_path = 'affixes'
unique_rarity_path = 'rarity/unique.png'
legendary_rarity_path = 'rarity/legendary.png'
hits_config_path = 'config/hits.txt'