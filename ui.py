import customtkinter
import tkinter
from functools import partial
import constants
from constants import ItemType
from PIL import Image

customtkinter.set_appearance_mode("Dark")
customtkinter.set_default_color_theme("green")

hits_array = open(constants.hits_config_path).read().splitlines()
hits_var_array = []

affixes_initial_values = {
    ItemType.helm: open(constants.item_type_config_dict[ItemType.helm]).read().splitlines(),
    ItemType.chest: open(constants.item_type_config_dict[ItemType.chest]).read().splitlines(),
    ItemType.weapon: open(constants.item_type_config_dict[ItemType.weapon]).read().splitlines(),
    ItemType.gloves: open(constants.item_type_config_dict[ItemType.gloves]).read().splitlines(),
    ItemType.pants: open(constants.item_type_config_dict[ItemType.pants]).read().splitlines(),
    ItemType.boots: open(constants.item_type_config_dict[ItemType.boots]).read().splitlines(),
    ItemType.ring: open(constants.item_type_config_dict[ItemType.ring]).read().splitlines(),
    ItemType.amulet: open(constants.item_type_config_dict[ItemType.amulet]).read().splitlines()
}

affixes_var_dict = {
    ItemType.helm: {},
    ItemType.chest: {},
    ItemType.weapon: {},
    ItemType.gloves: {},
    ItemType.pants: {},
    ItemType.boots: {},
    ItemType.ring: {},
    ItemType.amulet: {},
}


class BotTabView(customtkinter.CTkTabview):
    def __init__(self, master, **kwargs):
        super().__init__(master, **kwargs)

        for number in hits_array:
            hits_var_array.append(customtkinter.StringVar(value=number))

        for i in range(1, 9):
            item_type = ItemType(i)

            tab = item_type.name
            self.add(tab)

            self.hits_label = customtkinter.CTkLabel(
                master=self.tab(tab), text="Min number of affixes: "
            )
            self.hits_label.grid(row=0, column=0)

            a = customtkinter.StringVar(value=number)
            a.get()
            self.hits_menu = customtkinter.CTkOptionMenu(
                master=self.tab(tab),
                values=["1", "2", "3", "4"],
                variable=hits_var_array[i-1],
                command=save_config_hits,
            )
            self.hits_menu.grid(row=0, column=1)

            affixes = open(constants.item_type_affixes_list_dict[item_type]).read().splitlines()
            for affix in affixes:
                affixes_var_dict[item_type][affix] = (
                    customtkinter.BooleanVar(value=False)
                    if affixes_initial_values[item_type].count(affix) == 0
                    else customtkinter.BooleanVar(value=True)
                )
                checkbox = customtkinter.CTkCheckBox(
                    master=self.tab(tab),
                    text=affix,
                    variable=affixes_var_dict[item_type][affix],
                    command=partial(save_config_item, item_type),
                )
                checkbox.grid(sticky="NW")


def save_config_hits(v):
    string_list = []
    for value in hits_var_array:
        string_list.append(value.get())

    with open(constants.hits_config_path, "w") as tfile:
        tfile.write("\n".join(string_list))

def save_config_item(item_type: ItemType):
    string_list = get_affixes_for_item_type(item_type)
    with open(constants.item_type_config_dict[item_type], "w") as tfile:
        tfile.write("\n".join(string_list))

def get_affixes_for_item_type(item_type: ItemType):
    var_array = affixes_var_dict[item_type]
    string_list = []
    for key, value in var_array.items():
        if value.get():
            string_list.append(key)
    return string_list

def get_min_hits_for_item_type(item_type: ItemType):
    value = hits_var_array[item_type.value-1]
    return int(value.get())

class Ui(customtkinter.CTk):
    def __init__(self):
        super().__init__()

        img = Image.open('trash_men.png')
        img.putalpha(70)
        self.bg_image = customtkinter.CTkImage(img, size=(500, 400))#
        self.bg_image_label = customtkinter.CTkLabel(self, image=self.bg_image)
        self.bg_image_label.grid(row=0, column=0)

        self.title("Inventory garbage collector for trash game")
        self.tab_view = BotTabView(master=self)
        self.tab_view.grid(row=0, column=0, padx=20, pady=20)



