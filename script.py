import ui
import api
import keyboard
import constants

def run(keep_legendary):
    affixes_dict = {
        constants.ItemType.helm: ui.get_affixes_for_item_type(constants.ItemType.helm),
        constants.ItemType.chest: ui.get_affixes_for_item_type(constants.ItemType.chest),
        constants.ItemType.weapon: ui.get_affixes_for_item_type(constants.ItemType.weapon),
        constants.ItemType.gloves: ui.get_affixes_for_item_type(constants.ItemType.gloves),
        constants.ItemType.pants: ui.get_affixes_for_item_type(constants.ItemType.pants),
        constants.ItemType.boots: ui.get_affixes_for_item_type(constants.ItemType.boots),
        constants.ItemType.ring: ui.get_affixes_for_item_type(constants.ItemType.ring),
        constants.ItemType.amulet: ui.get_affixes_for_item_type(constants.ItemType.amulet),
    }
    min_hits_dict = {
        constants.ItemType.helm: ui.get_min_hits_for_item_type(constants.ItemType.helm),
        constants.ItemType.chest: ui.get_min_hits_for_item_type(constants.ItemType.chest),
        constants.ItemType.weapon: ui.get_min_hits_for_item_type(constants.ItemType.weapon),
        constants.ItemType.gloves: ui.get_min_hits_for_item_type(constants.ItemType.gloves),
        constants.ItemType.pants: ui.get_min_hits_for_item_type(constants.ItemType.pants),
        constants.ItemType.boots: ui.get_min_hits_for_item_type(constants.ItemType.boots),
        constants.ItemType.ring: ui.get_min_hits_for_item_type(constants.ItemType.ring),
        constants.ItemType.amulet: ui.get_min_hits_for_item_type(constants.ItemType.amulet),
    }
    api.scan_inventory(keep_legendary, affixes_dict, min_hits_dict, True)

keyboard.add_hotkey("ctrl + left", lambda: run("True"))
keyboard.add_hotkey("ctrl + right", lambda: run("False"))

app = ui.Ui()
app.mainloop()